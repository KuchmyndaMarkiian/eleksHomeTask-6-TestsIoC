using System;
using System.Collections.Generic;
using System.Linq;
using HT1.Files;
using HT1.Interfaces;
using Other;

namespace HT1.Datas
{
    public class Section : ISection, IComparable<Section>
    {
        private string name;
        private List<Author> authors;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public List<Author> Authors
        {
            get { return authors; }
            set { authors = value; }
        }

        public Section()
        {
        }

        public Section(string name, List<Author> authors)
        {
            this.name = name;
            this.authors = authors;
        }

        public override string ToString() => $"Section {name} which contains books such as:\n{authors.toString()}\n";
        public int GetBookCount() => authors.Select(a => a.Books.Count).Sum();

        public int CompareTo(Section other)
        {
            if (GetBookCount() > other.GetBookCount())
                return 1;
            if (GetBookCount() < other.GetBookCount())
                return -1;
            return 0;
        }
    }
}