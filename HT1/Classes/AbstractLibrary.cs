﻿using System;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using System.Linq;
using HT1.Datas;
using HT1.Interfaces;

namespace HT1.Files
{
    abstract class AbstractLibrary : IComparable<AbstractLibrary>, ICountingBooks
    {
        private string name;
        private List<Section> sections;
        public string Name => name;

        public List<Section> Sections
        {
            get { return sections; }
            set { sections = value; }
        }

        public override string ToString() => "Abstract library.";

        public AbstractLibrary(string name, List<Section> sections)
        {
            this.name = name;
            this.sections = sections;
        }

        public AbstractLibrary()
        {
        }

        public int CompareTo(AbstractLibrary other) => 0;

        public int GetBookCount()
            => sections.SelectMany(sections => sections.Authors).Sum(authors => authors.Books.Count);

        //Additional task#1 Author which wrote more books
        public Author GetAuthorWithMoreBooks()
            =>
                sections.Select(s => s.Authors.First(a => a.GetBookCount() == s.Authors.Max(a1 => a1.GetBookCount())))
                    .Last();

        //Additional task#2 Section which has more books
        public Section GetSectionWithMoreBooks()
            => sections.First(s => s.GetBookCount() == sections.Max(s1 => s1.GetBookCount()));

        //Additional task#3 Book which has less pages
        public Book GetBookWithLessPages()
        {
            Book result = new Book(null, int.MaxValue);
            foreach (var section in sections)
            {
                foreach (var author in section.Authors)
                {
                    if (author.Books.Any(b => b.CompareTo(result) < 0))
                    {
                        result = author.Books.FirstOrDefault(b => b.CompareTo(result) < 0);
                    }
                }
            }
            return result;
        }
    }
}
