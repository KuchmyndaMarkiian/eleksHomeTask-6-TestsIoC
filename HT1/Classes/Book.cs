using System;
using HT1.Interfaces;
using _1_Library.Interfaces;

namespace HT1.Datas
{
    public class Book : IBook, IComparable<Book>
    {
        private string name;
        private int pages;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Pages
        {
            get { return pages; }
            set { pages = value; }
        }

        public Book()
        {
        }

        public Book(string name, int pages)
        {
            this.name = name;
            this.pages = pages;
        }

        public override string ToString() => $"\'{name}\', {pages}p.";

        public int CompareTo(Book other)
        {
            if (pages > other.Pages)
                return 1;
            if (pages < other.Pages)
                return -1;
            return 0;
        }
    }
}