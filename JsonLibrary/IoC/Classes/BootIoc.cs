﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HT1.Datas;
using HT1.Files;
using HT1.Interfaces;
using JsonLibrary.IoC.Interfaces;
using SimpleInjector;

namespace JsonLibrary.IoC
{
    static class BootIoc
    {
        public static Container Container;
        
        public static void Run(ILibrary library)
        {
            Container=new Container();
            Container.Register<ILibrary>(()=>library);
            Container.Register<IInjectedClass>(() => new InjectedClass(Container.GetInstance<ILibrary>()));
            Container.Verify();
        }
    }
}
