﻿/*
 * SonarQube, open source software quality management tool.
 * Copyright (C) 2008-2013 SonarSource
 * mailto:contact AT sonarsource DOT com
 *
 * SonarQube is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * SonarQube is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

using System.Collections.Generic;
using System.Linq;
using HT1.Files;
using HT1.Interfaces;
using Other;

namespace HT1.Datas
{
    class Library : ILibrary
    {
        public Library(string name, List<ISection> sections)
        {
            Name = name;
            Sections = sections;
        }
        public  string toString() => $"Library \"{Name}\" with sections::\n\n{Sections.toString()}\n";       

        public string Name { get; set; }
        public List<ISection> Sections { get; set; }
        public List<IBook> GetBooks()
        {
            var res =new List<IBook>();
            foreach (var section in Sections)
            {
                foreach (var author in section.Authors)
                {
                    res.AddRange(author.Books);
                }
            }
            return res;
        }

        public List<IAuthor> GetAuthors()
        {
            var res = new List<IAuthor>();
            foreach (var section in Sections)
            {
                res.AddRange(section.Authors);
            }
            return res;
        }

        public List<ISection> GetSections()
        {
            return Sections;
        }

        public int GetBookCount() => Sections.SelectMany(sections => sections.Authors).Sum(authors => authors.Books.Count);
    }
}
