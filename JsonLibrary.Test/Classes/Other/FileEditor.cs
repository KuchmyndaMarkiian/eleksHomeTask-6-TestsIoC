﻿/*
 * SonarQube, open source software quality management tool.
 * Copyright (C) 2008-2013 SonarSource
 * mailto:contact AT sonarsource DOT com
 *
 * SonarQube is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * SonarQube is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

using System.IO;

namespace JsonLibrary.Other
{
    public class FileEditor
    {
        private FileStream _stream;
        public bool Flag { get; set; }

        public FileEditor()
        {
            _stream = null;
            Flag = true;
        }

        public void Write(string data)
        {
            WriteBytes(data.ToCharArray(), data.Length);
        }

        private void WriteBytes(char[] data, int length)
        {
            try
            {
                if (_stream != null)
                {
                    using (var _writer=new StreamWriter(_stream))
                    {
                        _writer.Write(data, 0, length);
                    }
                }
            }
            catch 
            {
                Flag = false;
            }
        }

        public string Read()
        {
            try
            {
                return ReadBytes();
            }
            catch
            {
                Flag = false;
                return null;
            }
        }

        private string ReadBytes()
        {
            if (_stream != null)
            {
                using (var _reader=new StreamReader(_stream))
                {
                    return _reader.ReadToEnd();
                }
            }
            return null;
        }

        public void OpenOrCreate(string name)
        {
            _stream = new FileStream(name, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            Flag = true;
        }


        ~FileEditor()
        {
            _stream?.Close();
        }
    }
}
