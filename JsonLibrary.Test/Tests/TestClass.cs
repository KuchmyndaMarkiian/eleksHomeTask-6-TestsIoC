﻿using System.Collections.Generic;
using System.Linq;
using HT1.Datas;
using HT1.Files;
using HT1.Interfaces;
using Moq;
using NUnit.Framework;

namespace JsonLibrary.Test.Tests
{

    class TestClass
    {
        [TestCase]
        //nUnit
        public void Book_Counting_nUnit()
        {
            //Arrange
            var library = Init();

            //Act
            var result = library.GetBookCount();

            //Assert
            Assert.IsTrue(result>0);
        }
        //Moq
        [TestCase]
        public void Get_Books_And_Is_Not_Null_Moq()
        {
            //Arrange
            var mock=new Mock<ILibrary>();
            mock.Setup(a => Init());
            mock.Setup(a => a.GetBooks()).Returns(new List<IBook>());
            var mocked = mock.Object;
            //Act
            var books = mocked.GetBooks();
            //Assert
            Assert.IsTrue(books.Any());
        }

        private ILibrary Init()
        {
            #region Init
            return new Library("LPNU Library", new List<ISection>
            {
                new Section("IT", new List<IAuthor>
                {
                    new Author("Straustrup & Shieldt", new List<IBook>
                    {
                        new Book("C++", 694),
                        new Book("C#", 534)
                    }),
                    new Author("Serdyuk", new List<IBook>
                    {
                        new Book("C#", 534),
                        new Book("KPZ", 300),
                        new Book("MPZ", 350)
                    })
                }),
                new Section("Ukrainan Literature", new List<IAuthor>
                    {
                        new Author("Taras Shevschenko", new List<IBook> {new Book("Kobzar", 800)})
                    }
                ),
                new Section("Foreighn Literature", new List<IAuthor>
                {
                    new Author("Oscar Wilde", new List<IBook> {new Book("Dorian Gray", 1000)}),
                    new Author("Aziz Ansari", new List<IBook> {new Book("How be single", 98)}),
                    new Author("Herbert Walles", new List<IBook>
                    {
                        new Book("Future", 666),
                        new Book("Future #2", 466)
                    })
                })
            });
            #endregion
        }
    }
}
